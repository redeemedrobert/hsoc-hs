export class CalendarService {
    startDate: Date

    cunstructor() {
        this.startDate = new Date()
    }

    public loadUpcomingEvents() {
        const form = new FormData()
        const date = new Date()
        form.append('req', 'getcal')
        form.append('date', this.date2String(date) + '0000')
        form.append('year', String(date.getFullYear()))
        form.append('month', String(date.getMonth() + 1))
        return this.ajaxRequest(form)
    }

    public date2String(date: Date) {
        const year = date.getFullYear()
        const month = date.getMonth()
        const day = date.getDate()
        return `${year}${month < 9 ? `0${month+1}` : month+1}${day < 10 ? `0${day}` : day}`
    }

    private ajaxRequest(data: FormData) {
        return new Promise((accept, reject) => {
            const req = new XMLHttpRequest()

            req.onload = function() {
                if(this.status === 200 && this.readyState === 4){
                    console.log(this.responseText)
                    accept(
                        JSON.parse(this.responseText)
                    )
                }
                if(this.status > 200) {
                    reject(
                        {
                            "res": { "error": `Error retrieving data from server: ${this.status}`  }
                        }
                    )
                }
            } 

            req.open('POST', 'index.php')
            req.send(data)
        })
    }
}
