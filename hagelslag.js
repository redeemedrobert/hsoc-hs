export class Hagel {
    //nodes: Array<string> = []
    constructor(root, state) {
        if (root.length)
            this.appRoot = document.getElementById(root);
    }
    static createElement(properties, appState) {
        let el = document.createElement(properties.tagName);
        if (properties.id)
            el.id = properties.id;
        if (properties.props && properties.props.length)
            for (let i = 0; i < properties.props.length; i += 2) {
                if (properties.props[i].substr(0, 1) === '$') {
                    if (appState) {
                        appState.registerElement(properties.id, properties.props[i].substr(1), properties.props[i + 1]);
                        if (properties.props[i].substr(1) === 'innerText')
                            el.innerText = appState.getVariable(properties.props[i + 1]);
                        else if (properties.props[i].substr(1) === 'innerHTML')
                            el.innerHTML = appState.getVariable(properties.props[i + 1]);
                        else
                            el.setAttribute(properties.props[i].substr(1), appState.getVariable(properties.props[i + 1]));
                    }
                    else
                        console.error(`Hagelslag error: tried to bind to a property, ${properties.props[i].substr(1)}, on ${properties.id} without including an appState in the call to createElement.`);
                }
                else if (properties.props[i].substr(0, 2) === '!$') {
                    if (appState) {
                        appState.registerElement(properties.id, properties.props[i].substr(2), properties.props[i + 1]);
                        if (properties.props[i].substr(2) === 'innerText')
                            el.innerText = appState.getVariable(properties.props[i + 1]);
                        else if (properties.props[i].substr(2) === 'innerHTML')
                            el.innerHTML = appState.getVariable(properties.props[i + 1]);
                        else
                            el.setAttribute(properties.props[i].substr(2), appState.getVariable(properties.props[i + 1]));
                        const inputTypeIndex = properties.props.indexOf('type');
                        const inputTypeValue = inputTypeIndex !== -1 ? inputTypeIndex + 1 : -1;
                        if (properties.tagName === 'textarea' ||
                            (inputTypeValue !== -1 && (properties.props[inputTypeValue] === 'text' ||
                                properties.props[inputTypeValue] === 'password'))) {
                            el.addEventListener('keyup', (e) => appState.updateVariable(properties.props[i + 1], e.currentTarget.value));
                        }
                        else {
                            el.addEventListener('change', (e) => appState.updateVariable(properties.props[i + 1], e.currentTarget.value));
                        }
                    }
                    else
                        console.error(`Hagelslag error: tried to bind to a property, ${properties.props[i].substr(2)}, on ${properties.id} without including an appState in the call to createElement.`);
                }
                else {
                    if (properties.props[i] === 'innerText')
                        el.innerText = properties.props[i + 1];
                    else if (properties.props[i] === 'innerHTML')
                        el.innerHTML = properties.props[i + 1];
                    else
                        el.setAttribute(properties.props[i], properties.props[i + 1]);
                }
            }
        if (properties.innerHTML)
            el.innerHTML = properties.innerHTML;
        if (properties.innerText)
            el.innerText = properties.innerText;
        if (properties.$innerHTML) {
            if (appState) {
                appState.registerElement(properties.id, 'innerHTML', properties.$innerHTML);
                el.innerHTML = appState.getVariable(properties.$innerHTML);
            }
            else
                console.error(`Hagelslag error: tried to bind to innerHTML on "${properties.id}" without including an appState in the call to createElement.`);
        }
        if (properties.$innerText) {
            if (appState) {
                appState.registerElement(properties.id, 'innerText', properties.$innerText);
                el.innerText = appState.getVariable(properties.$innerText);
            }
            else
                console.error(`Hagelslag error: tried to bind to innerText on "${properties.id}" without including an appState in the call to createElement.`);
        }
        if (properties.$classes) {
            if (appState) {
                properties.$classes.forEach(c => {
                    if (properties.id) {
                        appState.registerClass(c.name, c.className, c.condition, properties.id);
                    }
                    else
                        console.error(`Hagelslag error: tried to register class "${c.className}," but the element being registered to has no id.`);
                });
            }
            else
                console.error(`Hagelslag error: tried to bind a class on "${properties.id}" without including an appState in the call to createElement.`);
        }
        if (properties.children && properties.children.length)
            for (let i = 0; i < properties.children.length; ++i) {
                el.insertAdjacentElement('beforeend', properties.children[i]);
            }
        if (properties.events && properties.events.length)
            for (let i = 0; i < properties.events.length; i += 2) {
                el.addEventListener(properties.events[i], properties.events[i + 1]);
            }
        if (properties.for) {
            if (properties.template) {
                for (let i = 0; i < properties.for.length; ++i) {
                    el.insertAdjacentElement('beforeend', properties.template(properties.for[i]));
                }
            }
            else
                console.error(`Hagelslag error: tried to create an element, "${properties.id}," with a 'for' property without supplying a 'template' property.`);
        }
        if (properties.$for) {
            if (appState) {
                if (properties.template) {
                    appState.registerArrayElement(properties.id, properties.template, properties.$for);
                    appState.getArray(properties.$for).forEach((item) => {
                        el.insertAdjacentElement('beforeend', properties.template(item));
                    });
                }
                else
                    console.error(`Hagelslag error: tried to create an element, "${properties.id}," with a 'for' property without supplying a 'template' property.`);
            }
            else
                console.error(`Hagelslag error: tried to register element "${properties.id}" to array '${properties.$for}' without referencing a Slag instance in the function call.`);
        }
        return el;
    }
    static c(p, a) { return this.createElement(p, a); }
    append(el) {
        if (this.appRoot)
            this.appRoot.insertAdjacentElement('beforeend', el);
        else {
            this.appRoot = el;
        }
        //this.addIds(el)
    }
    a(el) { this.append(el); }
    remove(el) {
        el.remove();
        //this.removeIds(el)
    }
    r(el) { this.remove(el); }
    id(id) {
        if (document.getElementById(id))
            return document.getElementById(id);
        else
            console.error(`Hagelslag error: Unable to find element with ID ${id}.`);
    }
    export() {
        if (this.appRoot)
            return this.appRoot;
        else
            console.error('Hagelslag error: tried to export a class when no element has been appended to it.');
    }
    e() { return this.export(); }
}
export class Slag {
    constructor() {
        this.reactiveVariables = [];
        this.reactiveArrays = [];
    }
    registerElement(id, prop, variable) {
        let i = 0;
        while (i < this.reactiveVariables.length) {
            if (this.reactiveVariables[i].name === variable) {
                this.reactiveVariables[i].elements.push({ id: id, prop: prop });
                return this.reactiveVariables[i].value;
            }
            ++i;
        }
        console.error(`Hagelslag error: Could not find reactive variable named '${variable}.'`);
    }
    re(id, prop, variable) { return this.registerElement(id, prop, variable); }
    registerArrayElement(id, template, variable) {
        let i = 0;
        while (i < this.reactiveArrays.length) {
            if (this.reactiveArrays[i].name === variable) {
                this.reactiveArrays[i].elements.push({ id: id, template: template });
                return this.reactiveArrays[i].value;
            }
            ++i;
        }
        console.error(`Hagelslag error: Could not find reactive array named '${variable}.'`);
    }
    rae(id, template, variable) { return this.registerArrayElement(id, template, variable); }
    registerVariable(name, value) {
        if (this.reactiveVariables.every(i => i.name !== name)) {
            this.reactiveVariables.push({ name: name, type: typeof value, value: value, classes: [], elements: [] });
        }
        else
            console.error(`Hagelslag error: tried to register a reactive variable, ${name}, but there is already a registered variable with that name.`);
    }
    rv(name, value) { this.registerVariable(name, value); }
    registerClass(name, className, condition, id) {
        let found = false;
        this.reactiveVariables.forEach((v, i) => {
            if (v.name === name) {
                this.reactiveVariables[i].classes.push({ id: id, name: name, className: className, condition: condition });
                found = true;
            }
        });
        if (!found)
            console.error(`Hagelslag error: tried to register a class, "${className}" to variable "${name}", but a reactive variable with that name could not be found.`);
    }
    rc(name, className, condition, id) { this.registerClass(name, className, condition, id); }
    registerArray(name, value) {
        if (this.reactiveArrays.every(i => i.name !== name)) {
            this.reactiveArrays.push({ name: name, value: value, elements: [] });
        }
        else
            console.error(`Hagelslag error: tried to register a reactive array, ${name}, but there is already a registered variable with that name.`);
    }
    ra(name, value) { this.registerArray(name, value); }
    updateVariable(name, value) {
        this.reactiveVariables.forEach((rv) => {
            if (rv.name === name) {
                rv.value = value;
                for (let i = 0; i < rv.elements.length; i++) {
                    const elm = rv.elements[i];
                    if (elm.prop === 'innerHTML') {
                        document.getElementById(elm.id).innerHTML = rv.value;
                    }
                    else if (elm.prop === 'innerText') {
                        document.getElementById(elm.id).innerText = rv.value;
                    }
                    else if (elm.prop === 'value') {
                        document.getElementById(elm.id).value = rv.value;
                    }
                    else {
                        document.getElementById(elm.id).setAttribute(elm.prop, rv.value);
                    }
                }
                if (rv.classes)
                    rv.classes.forEach((c) => {
                        //classes?: Array<{id: string, className: string, condition: any}>
                        if (rv.value === c.condition && !document.getElementById(c.id).classList.contains(c.className)) {
                            document.getElementById(c.id).classList.add(c.className);
                        }
                        else if (document.getElementById(c.id).classList.contains(c.className)) {
                            document.getElementById(c.id).classList.remove(c.className);
                            if (!document.getElementById(c.id).getAttribute('class'))
                                document.getElementById(c.id).removeAttribute('class');
                        }
                    });
            }
        });
    }
    uv(name, value) { this.updateVariable(name, value); }
    updateArray(name, value, index) {
        this.reactiveArrays.forEach((rv) => {
            if (rv.name === name) {
                if (index !== undefined) {
                    rv.value[index] = value;
                    for (let i = 0; i < rv.elements.length; i++) {
                        const elm = document.getElementById(rv.elements[i].id);
                        elm.childNodes[index].replaceWith(rv.elements[i].template(value));
                    }
                }
                else {
                    const oldArray = rv.value;
                    rv.value = value;
                    for (let i = 0; i < rv.elements.length; i++) {
                        const elm = document.getElementById(rv.elements[i].id);
                        rv.value.forEach((item, ii) => {
                            if (ii < oldArray.length) {
                                if (JSON.stringify(item) != JSON.stringify(oldArray[ii])) {
                                    elm.childNodes[ii].replaceWith(rv.elements[i].template(item));
                                }
                            }
                            else {
                                elm.insertAdjacentElement('beforeend', rv.elements[i].template(item));
                            }
                        });
                        if (rv.value.length < oldArray.length) {
                            for (let ii = oldArray.length - 1; ii > rv.value.length - 1; --ii) {
                                elm.childNodes[ii].remove();
                            }
                        }
                    }
                }
            }
        });
    }
    ua(name, value, index) { this.updateArray(name, value, index !== undefined ? index : undefined); }
    getVariable(name) {
        let out = undefined;
        let found = false;
        this.reactiveVariables.forEach((rv) => {
            if (rv.name === name) {
                out = rv.value;
                found = true;
            }
        });
        if (!found)
            console.error(`Hagelslag warning: getVariable could not find a variable named '${name}.'`);
        else
            return out;
    }
    gv(name) { return this.getVariable(name); }
    getArray(name) {
        let out = undefined;
        let found = false;
        this.reactiveArrays.forEach((ra) => {
            if (ra.name === name) {
                out = ra.value;
                found = true;
            }
        });
        if (!found)
            console.error(`Hagelslag warning: getArray could not find a variable named '${name}.'`);
        else
            return out;
    }
    ga(name) { return this.getArray(name); }
}
export class Boter {
    constructor(outlet, routes, changeURL, basePath) {
        this.currentPage = '';
        this.outlet = outlet;
        if (routes)
            this.routes = routes;
        this.changeURL = changeURL ? true : false;
        if (basePath)
            this.basePath = basePath;
        if (basePath) {
            const currentPath = window.location.pathname.split('/');
            const baseSplit = basePath.split('/');
            const removeBase = currentPath.slice(baseSplit.length + 1, currentPath.length).join('/');
            if (changeURL)
                this.navigate(removeBase);
        }
        else if (changeURL)
            this.navigate(window.location.pathname);
    }
    addRoute(route) {
        this.routes.push(route);
    }
    removeRoute(route) {
        const rt = this.routes.find(element => element.route === route);
        if (rt) {
            this.routes = this.routes.filter(element => element.route !== route);
        }
        else
            console.error(`Hagelslag error: tried to remove a route, '${route}' that does not exist on the router that removeRoute was called on.`);
    }
    navigate(route) {
        let r = route.split('/');
        let matched = false;
        if (route === '') {
            const home = this.routes.find(element => element.home === true);
            if (home) {
                matched = true;
                while (document.getElementById(this.outlet).lastChild) {
                    document.getElementById(this.outlet).removeChild(document.getElementById(this.outlet).lastChild);
                }
                document.getElementById(this.outlet).insertAdjacentElement('afterbegin', home.element());
            }
        }
        else {
            for (let i = 0; i < this.routes.length; ++i) {
                let rt = this.routes[i].route.split('/');
                if (rt.length === r.length) {
                    let varPos = [];
                    let vars = [];
                    let rNoVars = [];
                    let rtNoVars = [];
                    rt.forEach((s, p) => {
                        if (s[0] === '$') {
                            varPos.push(p);
                            vars.push(r[p]);
                        }
                        else {
                            rNoVars.push(r[p].toLowerCase());
                            rtNoVars.push(rt[p].toLowerCase());
                        }
                    });
                    if (JSON.stringify(rNoVars) === JSON.stringify(rtNoVars)) {
                        matched = true;
                        while (document.getElementById(this.outlet).lastChild) {
                            document.getElementById(this.outlet).removeChild(document.getElementById(this.outlet).lastChild);
                        }
                        if (vars.length) {
                            let data = {};
                            for (let ii = 0; ii < varPos.length; ++ii) {
                                data[rt[varPos[ii]].substring(1)] = vars[ii];
                            }
                            document.getElementById(this.outlet).insertAdjacentElement('afterbegin', this.routes[i].element(data));
                        }
                        else {
                            document.getElementById(this.outlet).insertAdjacentElement('afterbegin', this.routes[i].element());
                        }
                        this.currentPage = route;
                        history.pushState(null, '', '/');
                        let historyBase = '';
                        if (this.basePath)
                            historyBase = this.basePath[0] === '/' ? this.basePath : '/' + this.basePath;
                        const historyRoute = route[0] === '/' ? route : '/' + route;
                        history.replaceState(null, '', historyBase + historyRoute);
                    }
                }
            }
        }
        if (!matched) {
            console.error(`Hagelslag error: attempted to navigate router on outlet '${this.outlet}' using a route that has not been registered.`, route);
            while (document.getElementById(this.outlet).lastChild) {
                document.getElementById(this.outlet).removeChild(document.getElementById(this.outlet).lastChild);
            }
            const notfound = this.routes.find(element => element.notfound === true);
            if (notfound)
                document.getElementById(this.outlet).insertAdjacentElement('afterbegin', notfound.element());
            else
                document.getElementById(this.outlet).insertAdjacentElement('afterbegin', Hagel.c({ tagName: 'div', innerText: '404, page not found' }));
        }
    }
}
