<?php
$db = new SQLite3("hsoc.db");
if(isset($_POST['req']))
{
  if($_POST['req'] == 'getcal')
  {
    $res = new stdClass;
    $res -> type = 'getcal';
    $res -> date = $_POST['date'];
    $res -> year = $_POST['year'];
    $res -> month = $_POST['month'];
    $events = $db -> query("SELECT * FROM calendar WHERE date>='" . $_POST['date'] . "' ORDER BY date LIMIT 5");
    $i = 0;
    //'calendar'('id' INTEGER PRIMARY KEY, 'title' TEXT, 'desc' TEXT, 'year' INTEGER, 'month' INTEGER, 'day' INTEGER, 'hour' INTEGER, 'minute' INTEGER)
    while($event = $events -> fetchArray())
    {
      $res -> events[$i] = array('id' => $event['id'], 'title' => $event['title'], 'desc' => $event['desc'], 'year' => $event['year'], 'month' => $event['month'], 'day' => $event['day'], 'hour' => $event['hour'], 'minute' => $event['minute']);
      $i++;
    }
    exit(json_encode($res));
  }
  if($_POST['req'] == 'loadcal')
  {
    $res = new stdClass;
    $res -> type = 'getcal';
    $res -> year = $_POST['year'];
    $res -> month = $_POST['month'];
    $events = $db -> query("SELECT * FROM calendar WHERE year='" . $_POST['year'] . "' AND month='" . $_POST['month'] . "' ORDER BY date");
    $i = 0;
    //'calendar'('id' INTEGER PRIMARY KEY, 'title' TEXT, 'desc' TEXT, 'year' INTEGER, 'month' INTEGER, 'day' INTEGER, 'hour' INTEGER, 'minute' INTEGER)
    while($event = $events -> fetchArray())
    {
      $res -> events[$i] = array('id' => $event['id'], 'title' => $event['title'], 'desc' => $event['desc'], 'year' => $event['year'], 'month' => $event['month'], 'day' => $event['day'], 'hour' => $event['hour'], 'minute' => $event['minute']);
      $i++;
    }
    exit(json_encode($res));
  }
  if($_POST['req'] == 'getblog' || $_POST['req'] == 'getfullblog')
  {
    $res = new stdClass;
    if($_POST['req'] == 'getblog') $res -> type = 'getblog';
    else $res -> type = 'getfullblog';
    $res -> offset = $_POST['offset'];
    $q = 'SELECT * FROM blog ORDER BY date DESC';
    if(isset($_POST['lim'])) $q.= ' LIMIT ' . $_POST['lim'];
    if($_POST['offset'] > 0) $q .= ' OFFSET ' .  $_POST['offset'];
    $query = $db -> query($q);
    $count = $query -> fetchArray();
    if(!$count || $_POST['offset'] < 0)
    {
      $res -> type = 'message';
      $res -> subject = 'noblog';
      $res -> message = 'No more blog posts found.';
      exit(json_encode($res));
    }
    $query2 = $db -> query($q);
    $rowcount = 0;
    while($post = $query2 -> fetchArray())
    {
      $aquery = $db -> query("SELECT name FROM users WHERE id='" . $post['author'] . "'");
      $aname = ($aquery -> fetchArray());
      if(!$aname && $post['author'] != 0)
      {
        $authorname = 'Name Unknown';
      }
      else
      {
        if($post['author'] == 0){ $authorname = 'Holy Spirit Orthodox Church'; }
        else { $authorname = $aname[0]; }
      }
      $res -> posts[$rowcount] = array('id' => $post['id'], 'authorid' => $post['author'], 'author' => $authorname, 'title' => $post['title'], 'content' => $post['content'], 'date' => $post['date']);
      $rowcount ++;
    }
    $rows = $db -> query("SELECT count(*) FROM blog");
    $res -> rows = $rows -> fetchArray()[0];
    $res -> offset = $_POST['offset'];
    exit(json_encode($res));
  }
  if($_POST['req'] == 'blogpost')
  {
    $query = $db -> query("SELECT * FROM blog WHERE id='" . $_POST['id'] . "'");
    $post = $query -> fetchArray();
    if($post)
    {
      $aquery = $db -> query("SELECT name FROM users WHERE id='" . $post['author'] . "'");
      $aname = ($aquery -> fetchArray());
      if(!$aname && $post['author'] != 0)
      {
        $authorname = 'Name Unknown';
      }
      else
      {
        if($post['author'] == 0){ $authorname = 'Holy Spirit Orthodox Church'; }
        else { $authorname = $aname[0]; }
      }
      $res = new stdClass;
      $res -> type = 'blogpost';
      $res -> id = $post['id'];
      $res -> authorid = $post['author'];
      $res -> author = $authorname;
      $res -> title = $post['title'];
      $res -> content = $post['content'];
      $res -> date = $post['date'];
      exit(json_encode($res));
    }
    else {
      $res = new stdClass;
      $res -> type = 'message';
      $res -> subject = 'blognotfound';
      $res -> message = 'There was no blog post found with the given ID :-(';
      exit(json_encode($res));
    }
  }
  if($_POST['req'] == 'getannouncement')
  {
    $res = new stdClass;
    $res -> type = 'getannouncement';
    $query = $db -> query("SELECT value FROM settings WHERE setting='ancontent'");
    $query2 = $db -> query("SELECT value FROM settings WHERE setting='antype'");
    $ancontent = $query -> fetchArray();
    $antype = $query2 -> fetchArray();
    $res -> ancontent = $ancontent[0];
    $res -> antype = $antype[0];
    exit(json_encode($res));
  }
  if($_POST['req'] == 'getresources')
  {
    $res = new stdClass;
    $res -> type = 'getresources';
    $res -> resources = array();
    $res -> parent = $_POST['parent'];
    $query = $db -> query("SELECT * FROM resources WHERE parent='" . $_POST['parent'] . "'");
    while($result = $query -> fetchArray())
    {
      $authorname = '';
      if($result['author'] == 0){
        $authorname = 'Holy Spirit Orthodox Church';
      }else{
        $authorquery = $db -> query("SELECT name FROM users WHERE id='" . $result['id'] . "'");
        $author = $authorquery -> fetchArray();
        $authorname = '';
        if($author){
          $authorname = $author[0];
        }else{
          $authorname = 'Name Not Available';
        }
      }
      array_push($res -> resources, array('id' => $result['id'], 'author' => $result['author'], 'authorname' => $authorname, 'title' => $result['title'], 'content' => $result['content'], 'date' => $result['date'], 'category' => $result['category'], 'parent' => $result['parent']));
    }
    exit(json_encode($res));
  }
  if($_POST['req'] == 'resource')
  {
    $query = $db -> query("SELECT * FROM resources WHERE id='" . $_POST['id'] . "'");
    $post = $query -> fetchArray();
    if($post)
    {
      $aquery = $db -> query("SELECT name FROM users WHERE id='" . $post['author'] . "'");
      $aname = ($aquery -> fetchArray());
      if(!$aname && $post['author'] != 0)
      {
        $authorname = 'Name Unknown';
      }
      else
      {
        if($post['author'] == 0){ $authorname = 'Holy Spirit Orthodox Church'; }
        else { $authorname = $aname[0]; }
      }
      $res = new stdClass;
      $res -> type = 'resource';
      $res -> id = $post['id'];
      $res -> authorid = $post['author'];
      $res -> authorname = $authorname;
      $res -> title = $post['title'];
      $res -> content = $post['content'];
      $res -> date = $post['date'];
      exit(json_encode($res));
    }
    else {
      $res = new stdClass;
      $res -> type = 'message';
      $res -> subject = 'resourcenotfound';
      $res -> message = 'There was no resource found with the given ID :-(';
      exit(json_encode($res));
    }
  }
}
?>
<html>
<title>Holy Spirit Orthodox Church // Venice, FL</title>
<meta name='viewport' content='width=device-width, initial-scale=1' />
<link rel='stylesheet' type='text/css' href='theme.css' />
<script type='module' src='main.js'></script>
<script src="https://tithe.ly/widget/v3/give.js?3"></script>
<body>
  <div class='header'>
    <div class='headertext'>
      <h1 class='headertitle'>Holy Spirit Orthodox Church // Venice, Fl.</h1>
    </div>
  </div>
  <div id="app-container"></div>
  <script>window.onload = ()=> { var tw = create_tithely_widget(); }</script>
  <div class='footer'>
    <br /><small>Holy Spirit Orthodox Church - Venice, FL - Orthodox Church in America/Diocese of Dallas and the South</small><br />
  </div>
</body>
</html>
