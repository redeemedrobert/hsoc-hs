import { Hagel, Boter } from './hagelslag.js'
import { state } from './state.js'
import { appDom } from './components/app.js'
import { home } from './components/home.js'
import { CalendarService } from './services/calendar.js'

export const calendarService = new CalendarService()
const app = new Hagel('app-container')
app.a(Hagel.c(appDom))

export const router = new Boter('outlet', [
    { route: '', element: ()=> Hagel.c(home), home: true },
    { route: 'calendar', element: ()=> Hagel.c(home) },
    { route: 'news', element: ()=> Hagel.c(home) },
    { route: 'resources', element: ()=> Hagel.c(home) },
    { route: '404', element: ()=> Hagel.c(home), notfound: true },
], true, 'hsoc-hs')

window.onload = () => {
    calendarService.loadUpcomingEvents().then(
        (res: {events: Array<any>}) => {
            state.updateArray('upcomingEvents', res.events)
        },
        (err) => console.error(err)
    )
}
