import { Hagel } from '../../hagelslag.js'

export const event = (data:any) => {
    return data ? Hagel.c({
        tagName: 'div',
        props: [ 'class', 'eventcontainer' ],
        children: [
            Hagel.c({
                tagName: 'div',
                props: [ 'class', 'eventanchorl' ],
                innerText: `${data.month+1}/${data.day}/${data.year}: `
            }),
            Hagel.c({
                tagName: 'div',
                props: [ 'class', 'eventanchorr' ],
                innerText: data.title
            }),
            Hagel.c({
                tagName: 'div',
                id: `eventdesc${data.id}`,
                props: [ 'class', 'eventdesc' ],
                innerText: data.desc
            })
        ],
        events: ['mouseup', ()=>{
            if(document.getElementById(`eventdesc${data.id}`).style.display === 'block'){
                document.getElementById(`eventdesc${data.id}`).style.display = 'none'
            } else document.getElementById(`eventdesc${data.id}`).style.display = 'block'
        }]
    }) : Hagel.c({
        tagName: 'div',
        props: [ 'class', 'eventcontainer' ],
        innerText: 'No upcoming events to show.'
    })
}
