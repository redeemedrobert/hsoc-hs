import { Hagel, ElementProperties } from '../hagelslag.js'
import { nav } from './nav.js'

export const appDom: ElementProperties = {
    tagName: 'div',
    id: 'app-root',
    props: [ 'class', 'content' ],
    children: [
        Hagel.c(nav),
        Hagel.c({ tagName: 'div', id: 'outlet' })
    ]
}
