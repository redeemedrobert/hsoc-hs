import { Hagel } from '../hagelslag.js';
export const nav = {
    tagName: 'div',
    props: ['class', 'nav'],
    id: 'nav',
    children: [
        Hagel.c({
            tagName: 'a',
            innerText: 'Welcome'
        }),
        Hagel.c({
            tagName: 'a',
            innerText: 'Calender'
        }),
        Hagel.c({
            tagName: 'a',
            innerText: 'News'
        }),
        Hagel.c({
            tagName: 'a',
            innerText: 'Resources'
        }),
        Hagel.c({
            tagName: 'button',
            props: ['class', 'tithely-give-btn', 'data-church-id', '75176'],
            innerText: 'Give'
        })
    ]
};
