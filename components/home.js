import { Hagel } from '../hagelslag.js';
import { state } from '../state.js';
export const home = {
    tagName: 'div',
    id: 'homecontainer',
    children: [
        Hagel.c({
            tagName: 'span',
            id: 'servicetimes',
            props: ['style', 'display: block; font-weight: bold; margin-top: 3; text-align: center;'],
            innerText: 'Sunday Liturgy: 10AM, Saturday Vespers: 5PM'
        }),
        Hagel.c({
            tagName: 'h3',
            innerText: "Hello! We're Holy Spirit Orthodox Church and we're..."
        }),
        Hagel.c({
            tagName: 'div',
            props: ['class', 'iconFlex'],
            children: [
                Hagel.c({
                    tagName: 'div',
                    props: ['class', 'icon'],
                    children: [
                        Hagel.c({
                            tagName: 'img',
                            props: ['src', 'res/hello.svg']
                        }),
                        Hagel.c({
                            tagName: 'h4',
                            innerText: 'Glad to meet you!'
                        }),
                        Hagel.c({
                            tagName: 'p',
                            innerText: "We're friendly. Everyone is welcome, whether you're an Orthodox Christian, other Christian, or not a Christian at all, we'd love to meet you."
                        })
                    ]
                }),
                Hagel.c({
                    tagName: 'div',
                    props: ['class', 'icon'],
                    children: [
                        Hagel.c({
                            tagName: 'img',
                            props: ['src', 'res/cross.svg']
                        }),
                        Hagel.c({
                            tagName: 'h4',
                            innerText: 'Orthodox'
                        }),
                        Hagel.c({
                            tagName: 'p',
                            innerText: "We're part of the Orthodox Church in America and we live and breathe the eastern tradition of the church. Come worship with us!"
                        })
                    ]
                }),
                Hagel.c({
                    tagName: 'div',
                    props: ['class', 'icon'],
                    children: [
                        Hagel.c({
                            tagName: 'img',
                            props: ['src', 'res/plant.svg']
                        }),
                        Hagel.c({
                            tagName: 'h4',
                            innerText: 'Growing'
                        }),
                        Hagel.c({
                            tagName: 'p',
                            innerText: "From our roots as a small OCA mission in Venice, we're growing into a vibrant community of people who love Christ and each other. Come grow with us!"
                        })
                    ]
                }),
            ]
        }),
        Hagel.c({
            tagName: 'h3',
            innerHTML: "We should try to live in such a way that, if the Gospels were lost, they could be rewritten by looking at us. <br />-Metropolitan Anthony Bloom"
        }),
        Hagel.c({
            tagName: 'div',
            props: ['class', 'map'],
            children: [
                Hagel.c({
                    tagName: 'div',
                    props: ['class', 'mapinfo'],
                    children: [
                        Hagel.c({
                            tagName: 'div',
                            props: ['class', 'maptext'],
                            children: [
                                Hagel.c({ tagName: 'h4', props: ['class', 'maptextitem'], innerText: 'Come visit us!' }),
                                Hagel.c({ tagName: 'p', props: ['class', 'maptextitem'], innerText: 'Ph: 123-456-7890' }),
                                Hagel.c({ tagName: 'p', props: ['class', 'maptextitem'], children: [
                                        Hagel.c({ tagName: 'a', props: ['href', 'mailto:hello@hsoc-venice.com'], innerText: 'hello@hsoc-venice.com' })
                                    ] }),
                                Hagel.c({ tagName: 'p', props: ['class', 'maptextitem'], innerText: '700 Shamrock Blvd, Venice, FL 34293' }),
                                Hagel.c({ tagName: 'p', props: ['class', 'maptextitem'], innerText: 'Sunday Divine Liturgy: 10AM' })
                            ]
                        }),
                        Hagel.c({
                            tagName: 'div',
                            children: [
                                Hagel.c({
                                    tagName: 'iframe',
                                    props: ['src', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3552.6214006982987!2d-82.40299968527248!3d27.07369598305856!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88c35a1a9f5af001%3A0x89f6ccdeac114e12!2sHoly%20Spirit%20Orthodox%20Church!5e0!3m2!1sen!2sus!4v1570652298733!5m2!1sen!2sus', 'frameborder', '0', 'allowfullscreen', '']
                                })
                            ]
                        })
                    ]
                })
            ]
        }),
        Hagel.c({
            tagName: 'h3',
            innerText: 'Upcoming Events'
        }),
        Hagel.c({
            tagName: 'div',
            id: 'events',
            $for: 'upcomingEvents',
            template: (ev) => Hagel.c({ tagName: 'div', innerText: ev.title })
        }, state),
        Hagel.c({
            tagName: 'h3',
            innerText: 'Latest News'
        }),
        Hagel.c({
            tagName: 'div',
            id: 'blog',
            innerText: 'No news to show.'
        })
    ]
};
